extern crate term_bg_color;

use term_bg_color::get_background_color;

fn main() {
    let color = get_background_color();
    println!("Color: {:?}", color);
}
