#[cfg(unix)]
extern crate libc;

#[cfg(unix)]
mod unix;

#[cfg(windows)]
extern crate winapi;

#[cfg(windows)]
mod win;

/// Contains the color found by `get_background_color`
#[derive(PartialEq, Debug)]
pub struct Color {
    pub red: u8,
    pub green: u8,
    pub blue: u8,
}

impl Color {
    /// Build a new color
    pub fn new(red: u8, green: u8, blue: u8) -> Color {
        Color { red, green, blue }
    }
}

/// Return the background color of the current terminal.
pub fn get_background_color() -> Option<Color> {
    #[cfg(unix)]
    return unix::get_background_color();

    #[cfg(windows)]
    return win::get_background_color();
}
