use super::Color;
use libc::{self, ECHO, ICANON, STDIN_FILENO, TCSAFLUSH, TCSANOW, tcsetattr};
use std::mem;
use std::str;
use std::u64;

const QUERY_COLOR: &[u8] = b"\x1b]11;?\x07";

const READ_TIMEOUT_MS: libc::c_int = 250;

pub fn get_background_color() -> Option<Color> {

    let result;

    unsafe {
        // If there are data available in standard input we abort the
        // operation, to avoid data loss.
        if can_read_stdin(0) {
            return None;
        }

        // Get the current attributes to disable just buffering and echo.
        let mut attrs: libc::termios = mem::zeroed();
        if libc::tcgetattr(STDIN_FILENO, &mut attrs) == -1 {
            return None;
        }

        let original_attrs = attrs;

        attrs.c_lflag &= !(ICANON | ECHO);
        if tcsetattr(STDIN_FILENO, TCSAFLUSH, &attrs) == -1 {
            return None;
        }

        result = write_code();

        // Restore attributes. Ignore errors since we can't recover at this
        // point.
        let _ = tcsetattr(STDIN_FILENO, TCSANOW, &original_attrs);
    }

    result
}

unsafe fn write_code() -> Option<Color> {
    // Write the sequence code to get the background color. If the sequence
    // is not written in one call, we assume that the standard output
    // buffer is full, so the operation is aborted.
    if libc::write(
        libc::STDOUT_FILENO,
        QUERY_COLOR.as_ptr() as *const _,
        QUERY_COLOR.len(),
    ) != QUERY_COLOR.len() as isize
    {
        return None;
    }

    // Wait up to READ_TIMEOUT_MS milliseconds to get the response from the TTY.
    if !can_read_stdin(READ_TIMEOUT_MS) {
        return None;
    }

    let mut input = [0u8; 64];
    let input_len = libc::read(STDIN_FILENO, input.as_mut_ptr() as *mut _, input.len());
    if input_len > 0 {
        return parser_color(&input[0..(input_len as usize)]);
    }

    None
}

unsafe fn can_read_stdin(timeout: libc::c_int) -> bool {
    let mut fds = [
        libc::pollfd {
            fd: STDIN_FILENO,
            events: libc::POLLIN,
            revents: 0,
        },
    ];

    let res = libc::poll(fds.as_mut_ptr(), fds.len() as libc::nfds_t, timeout);
    if res != fds.len() as libc::c_int {
        return false;
    }

    fds[0].revents & libc::POLLIN != 0
}

// Parse a color like `\e]11;rgb:RRRR/GGGG/BBBB\007`. Every part can have any
// number of characters. The parse will adjust it to a u8 value.
fn parser_color(data: &[u8]) -> Option<Color> {
    if !(data.starts_with(b"\x1b]11;rgb:") && data.ends_with(b"\x07")) {
        return None;
    }

    let data = &data[9..data.len() - 1];
    let mut parts = data.split(|&c| c == b'/');

    macro_rules! c {
        ($p: expr) => {
            match $p.next() {
                None => return None,
                Some(c) => {
                    let value = match str::from_utf8(c).map(|s| u64::from_str_radix(s, 16)) {
                        Ok(Ok(v)) => v,
                        _ => return None,
                    };
                    let max_value: u64 = (0..c.len()).fold(0, |acc, _| acc << 4 | 0xF);
                    ((255 * value) / max_value) as u8
                }
            }
        }
    }

    Some(Color::new(c!(parts), c!(parts), c!(parts)))
}

#[test]
fn test_parse_color() {
    assert_eq!(
        parser_color(b"\x1b]11;rgb:ffff/ffff/ffff\x07"),
        Some(Color::new(255, 255, 255))
    );
    assert_eq!(
        parser_color(b"\x1b]11;rgb:0000/0000/0000\x07"),
        Some(Color::new(0, 0, 0))
    );
    assert_eq!(
        parser_color(b"\x1b]11;rgb:33/66/99\x07"),
        Some(Color::new(0x33, 0x66, 0x99))
    );
    assert_eq!(parser_color(b"\x1b]11;rgb:xx/yy/zz\x07"), None);
    assert_eq!(parser_color(b"\x1b]11;rgb:00\x07"), None);
}
