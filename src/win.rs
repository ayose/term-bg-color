use std::mem;
use super::Color;
use winapi::um::handleapi::INVALID_HANDLE_VALUE;
use winapi::um::processenv::GetStdHandle;
use winapi::um::winbase::STD_OUTPUT_HANDLE;
use winapi::um::wincon::*;

pub fn get_background_color() -> Option<Color> {
    let mut info = unsafe { mem::zeroed() };

    let handle = unsafe { GetStdHandle(STD_OUTPUT_HANDLE) };
    if handle == INVALID_HANDLE_VALUE {
        return None;
    }

    let res = unsafe { GetConsoleScreenBufferInfo(handle, &mut info) };
    if res != 1 {
        return None;
    }

    let mut r = 0;
    let mut g = 0;
    let mut b = 0;

    if info.wAttributes & BACKGROUND_RED != 0 {
        r = 128;
    }

    if info.wAttributes & BACKGROUND_GREEN != 0 {
        g = 128;
    }

    if info.wAttributes & BACKGROUND_BLUE != 0 {
        b = 128;
    }

    if info.wAttributes & BACKGROUND_INTENSITY != 0 {
        r += 127;
        g += 127;
        b += 127;
    }

    Some(Color::new(r, g, b))
}
